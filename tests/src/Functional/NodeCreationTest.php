<?php

namespace Drupal\Tests\revealjs_node\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\node\Functional\NodeTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group revealjs_node
 */
class NodeCreationTest extends NodeTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['revealjs_node', 'block'];

  /**
   * List of neccessary modules.
   *
   * @var array
   */
  private static $neededModules = [
    'revealjs',
    'revealjs_node',
    'node',
    'ckeditor',
    'block',
    'options',
    'node_test_exception',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission create presentations.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->user = $this->drupalCreateUser([
      'create reveal_js_presentation content',
      'edit own reveal_js_presentation content',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Creates a "Reveal JS Presentation" node.
   */
  public function xtestNodeCreation() {

    $edit = [
      'title[0][value]' => $this->randomString(8),
      'field_section[0][value]' => $this->randomString(32),
    ];

    $this->drupalGet('node/add/reveal_js_presentation');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm($edit, 'Save');

    $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertNotEmpty($node, 'Node found in database.');

    $this->assertSession()->pageTextContains($edit['title[0][value]']);

    $this->assertSession()->elementExists(
      'xpath',
      '//div[contains(@class, "reveal")]//div[contains(@class, "slides")]//section[contains(@id,"page-1")]'
    );

    $this->assertSession()->elementNotExists(
      'xpath',
      '//div[contains(@class, "reveal")]//div[contains(@class, "slides")]//section[contains(@id,"page-2")]'
    );

  }

  /**
   * Creates a "Reveal JS Presentation" node.
   */
  public function testNodeCreation() {
    $helper = \Drupal::service('revealjs_node.helper');
    $settings =
      [
        'type' => $helper::BUNDLE,
        'title' => $this->randomString(8),
        'field_section' => [
          'value' => '<h1>test</h1>',
          'format' => $helper::BUNDLE,
        ],
        'presentation_theme' => ['value' => 'beige'],
        'uid' => \Drupal::currentUser()->id(),
      ];
    $node = Node::create($settings);
    $node->save();

    $node = $this->drupalGetNodeByTitle($settings['title']);
    $this->assertNotEmpty($node, 'Node found in database.');

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains($settings['title']);

    $this->assertSession()->elementExists(
      'xpath',
      '//div[contains(@class, "reveal")]//div[contains(@class, "slides")]//section[contains(@id,"page-1")]'
    );
  }

}
